@extends('sb-admin/app')

@section('title','Dashboard')

@section('content')
     <!-- Page Heading -->
     <h1 class="h3 mb-4 text-gray-800">Dashboard</h1>

 <!-- Content Row -->
 <div class="row">

     <!-- Earnings (Monthly) Card Example -->
     <div class="col-xl-3 col-md-6 mb-4">
         <div class="card border-left-primary shadow h-100 py-2">
             <div class="card-body">
                 <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                         <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                             Pasien Ibu Hamil</div>
                         <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_ibu_hamil}}</div>
                     </div>
                     <div class="col-auto">
                         {{-- <i class="fas fa-calendar fa-2x text-gray-300"></i> --}}
                         <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Earnings (Monthly) Card Example -->
     <div class="col-xl-3 col-md-6 mb-4">
         <div class="card border-left-success shadow h-100 py-2">
             <div class="card-body">
                 <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                         <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                             Pasien Ibu Melahirkan</div>
                         <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_ibu_melahirkan}}</div>
                     </div>
                     <div class="col-auto">
                         {{-- <i class="fas fa-dollar-sign fa-2x text-gray-300"></i> --}}
                         <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Earnings (Monthly) Card Example -->
     <div class="col-xl-3 col-md-6 mb-4">
         <div class="card border-left-info shadow h-100 py-2">
             <div class="card-body">
                 <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                         <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Pemeriksaan KB
                         </div>
                         <div class="row no-gutters align-items-center">
                             <div class="col-auto">
                                 <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">{{$jumlah_pemeriksaan_kb}}</div>
                             </div>
                         </div>
                     </div>
                     <div class="col-auto">
                         <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                     </div>
                 </div>
             </div>
         </div>
     </div>

     <!-- Pending Requests Card Example -->
     <div class="col-xl-3 col-md-6 mb-4">
         <div class="card border-left-warning shadow h-100 py-2">
             <div class="card-body">
                 <div class="row no-gutters align-items-center">
                     <div class="col mr-2">
                         <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                             Pemeriksaan Anak</div>
                         <div class="h5 mb-0 font-weight-bold text-gray-800">{{$jumlah_pemeriksaan_anak}}</div>
                     </div>
                     <div class="col-auto">
                         {{-- <i class="fas fa-comments fa-2x text-gray-300"></i> --}}
                         <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </div>



 {{-- Table Pengunjung --}}

 <div class="card-body">
     <div class="table-responsive">
         <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
             <thead>
                 <tr>
                     <th>No</th>
                     <th>Nama Pasien</th>
                     <th>Jenis Tindakan</th>
                     <th>Tanggal Pemeriksaan</th>
                     <th>Keterangan</th>
                  
                 </tr>
             </thead>
             <tfoot>
                 <tr>
                     <th>Name</th>
                     <th>Position</th>
                     <th>Office</th>
                     <th>Age</th>
                     <th>Start date</th>
                 
                 </tr>
             </tfoot>
             <tbody>
                 <tr>
                     <td>Tiger Nixon</td>
                     <td>System Architect</td>
                     <td>Edinburgh</td>
                     <td>61</td>
                     <td>2011/04/25</td>
                  
                 </tr>
                 <tr>
                     <td>Garrett Winters</td>
                     <td>Accountant</td>
                     <td>Tokyo</td>
                     <td>63</td>
                     <td>2011/07/25</td>
                   
                 </tr>
                 <tr>
                     <td>Ashton Cox</td>
                     <td>Junior Technical Author</td>
                     <td>San Francisco</td>
                     <td>66</td>
                     <td>2009/01/12</td>
                    
                 </tr>


             </tbody>
         </table>
     </div>
 </div>


 <!-- Content Row -->
@endsection
